<?php 
	$main_id = "home";
	$root = "";
	include('header.php');
?>
	<div class="wrap">
		<h2>¡Como para no bailar!</h2>
		<div class="player">
			<a href="#" class="cover_link" style="background-image:url(assets/images/foto_home.jpg);">
				<div class="player_icon"></div>
			</a>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/IhA3tz1L8Sg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
<?php include('footer.php'); ?>