<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, width=device-width, target-densitydpi=device-dpi, minimal-ui">
        <title>Memoria Anual 2017, IKEA en España</title>

        <meta name="title" content="Memoria Anual 2017, IKEA en España" />
        <meta name="description" content="Memoria Anual 2017, IKEA en España">
        <meta name="keywords" content="">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />

	    <meta property="og:url"            content="">
	    <meta property="og:title"          content="Memoria Anual 2017, IKEA en España">
	    <meta property="og:description"    content="Memoria Anual 2017, IKEA en España">
	    <meta property="og:image"          content="">

	    <link rel="stylesheet" href="<?php echo $root; ?>assets/css/style.css">
	    <script src="<?php echo $root; ?>assets/js/jquery-2.2.4.min.js"></script>
	    <script src="<?php echo $root; ?>assets/js/main.js"></script>
</head>
<body id="<?php echo $main_id; ?>" class="debugg">
	<div id="main">
		<div id="header">
			<div class="wrap">
				<a href="#" class="logo" target="_blank">Ikea</a>
				<h1>Memoria Anual 2017, IKEA en España</h1>
				<ul class="social">
					<li class="item_01"><a href="#" target="_blank">email</a></li>
					<li class="item_02"><a href="#" target="_blank">link</a></li>
					<li class="item_03"><a href="#" target="_blank">linkedin</a></li>
				</ul>
			</div>
		</div>
		<div class="wrap_menu wrap">
			<a href="#" class="mobile_menu">Menu</a>
			<div class="main_nav">
				<a href="<?php echo $root; ?>" class="item_01">Mensaje IKEA</a>
				<a href="<?php echo $root; ?>ikeaencifras/" class="item_02">IKEA en cifras</a>
				<a href="<?php echo $root; ?>ikeaylaspersonas/" class="item_03">IKEA y las personas</a>
				<a href="#" class="item_04">IKEA y el planeta</a>
				<a href="#" class="item_05">Sala de prensa</a>
			</div>
		</div>
		<div id="content">