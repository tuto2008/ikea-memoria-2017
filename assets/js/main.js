$(window).resize(ajustar);
$(window).bind('orientationchange', ajustar);

$(function(){
	$('.mobile_menu').on('click',function(){
		$('.main_nav').slideToggle(400);
	});
	$('.player .cover_link').on('click',function(){
		var iframe = $(this).closest('div.player').find('iframe');
		$(this).fadeOut(400);
		iframe.attr('src',iframe.attr('src') + '?autoplay=1');
		return false;
	});
	ajustar();
});

function ajustar(){
	$w 			= $(window).width();
	if($w >800){
		$('.main_nav').show();
	} else {
		$('.main_nav').hide();		
	}
}