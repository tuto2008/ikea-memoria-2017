<?php 
	$main_id = "ikeaylaspersonas";
	$root = "../";
	include('../header.php');
?>
	<div id="currentPost" class="active_02">
		<div class="wrap">
			<div class="main_block">
				<div class="text">
					<p>Esperamos que nuestros logros en diversidad, corresponsabilidad e igualdad sirvan como ejemplo para otras empresas y la sociedad en general.</p> 
					<p>Queremos alcanzar nuestras metas y seguir contribuyendo a crear un mejor día a día para la mayoría de las personas.</p>
				</div>
			</div>
		</div>
		<div class="wrap">
			<div class="secondary_block">
				<h2 class="color_01">Equipo</h2>
				<a href="#" class="download">Descargar <span class="icon-download"></span></a>
			</div>
			<div class="carrusel_block">
				<div class="nav">
					<a href="" class="preview"><span class="icon-arrow_l"></span> Anterior</a>
					<a href="" class="next">Siguiente <span class="icon-arrow_r"></span></a>
					<ul class="pagination">
						<li class="item1"><a href="#" class="active">page 01</a></li>
						<li class="item2"><a href="#">page 02</a></li>
						<li class="item3"><a href="#">page 03</a></li>
						<li class="item4"><a href="#">page 04</a></li>
					</ul>
				</div>
				<div class="content_carrusel">
					<ul class="carrusel_item page_01">
						<li class="con_01">
							<span class="icon icon-people"></span>
							<p>Somos <span>8.676</span> personas trabajando en IKEA España</p> 
						</li>
						<li class="con_02">
							<span class="icon icon-euro"></span>
							<p>Invertimos <span>168,5</span> millones en salarios</p>
						</li>
					</ul>
					<ul class="carrusel_item page_02">
						<li class="con_01">
							<span class="icon icon-wheel"></span>
							<p><span><strong>13</strong> mujeres</span> dirigen nuestras 17 tiendas entre directoras y subdirectoras</p>
						</li>
						<li class="con_02">
							<span class="icon icon-chupete"></span>
							<p><span><strong>52</strong> trabajadores y padres</span> han disfrutado de una  excedencia para cuidar  de sus hijos/as</p>
							<p class="info">*Desde 2010</p>
						</li>
						<li class="con_03">
							<span class="icon icon-pregnant"></span>
							<p><span>38</span> mujeres fueron contratadas estando embarazadas</p>
							<p class="info">*Desde 2010</p>
						</li>
						<li class="con_04">
							<span class="icon icon-woman"></span>
							<p><span>59,7%</span> de nuestro equipo son mujeres</p>
						</li>
						<li class="con_05">
							<span class="icon icon-woman"></span>
							<p><span>51,6%</span> de los puestos de mando están ocupados por mujeres</p>
						</li>
					</ul>
					<ul class="carrusel_item page_03"></ul>
					<ul class="carrusel_item page_04"></ul>
				</div>
			</div>
		</div>
	</div>
<?php include('../footer.php'); ?>