<?php 
	$main_id = "ikeaencifras";
	$root = "../";
	include('../header.php');
?>
	<div class="wrap">
		<div class="main_block">
			<div class="text">
				<p>Desde IKEA queremos seguir impulsando y dinamizando la economía y sociedad españolas.</p> 
				<p>Trabajamos con proveedores locales y brindamos soluciones que aportan un cambio positivo y significativo en la vida de las personas, mejorando su día a día</p>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="secondary_block">
			<h2 class="color_01">Datos Económicos</h2>
			<a href="#" class="download">Descargar <span class="icon-download"></span></a>
		</div>
		<div class="carrusel_block">
			<div class="nav">
				<a href="" class="preview"><span class="icon-arrow_l"></span> Anterior</a>
				<a href="" class="next">Siguiente <span class="icon-arrow_r"></span></a>
				<ul class="pagination">
					<li class="item1"><a href="#" class="active">page 01</a></li>
					<li class="item2"><a href="#">page 02</a></li>
					<li class="item3"><a href="#">page 03</a></li>
					<li class="item4"><a href="#">page 04</a></li>
				</ul>
			</div>
			<div class="content_carrusel">
				<ul class="carrusel_item page_01">
					<li class="con_01">
						<span class="icon icon-euro"></span>
						<p>Facturamos <span>1.466</span> millones de €</p>
					</li>
					<li class="con_02">
						<span class="icon icon-cart"></span>
						<p><span>66</span> millones de € se facturaron en IKEA Food</p>
					</li>
					<li class="con_03">
						<span class="icon icon-euro"></span>
						<p>Invertimos <span>21,1</span> millones de € en nuevas instalaciones y mejoras</p>
					</li>
					<li class="con_04">
						<span class="icon icon-shop"></span>
						<p>Tenemos <span>17 tiendas y 2 centros</span> de distribución</p>
					</li>
				</ul>
				<ul class="carrusel_item page_02"></ul>
				<ul class="carrusel_item page_03"></ul>
				<ul class="carrusel_item page_04"></ul>
			</div>
		</div>
	</div>
<?php include('../footer.php'); ?>